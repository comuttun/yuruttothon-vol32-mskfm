$(function(){
  var data = {
    messages: []
  };
	var game = {
		is_solo: true,
		clear:function(){
			console.log("game_clear");
      if(stageindex ==  0){
        stageindex = 1;
        stageinit();
      }else{
        //終わり
        gameover();
      }
		},
		send:function(data){
			send(data);
		}
	};
	//現在のstageのindex保存
	//初期値-1で
	var stageindex = -1;

  function gameover(){
    $(".ui-pagechange").show();
  }

	//stagelist
	var stagelist = [
		stage0(),
		stage1()
	];

	//dom element cache
	var elm = {
    body:$("body"),
		container:$(".container"),
		solo:$(".btn-solo"),
		multi:$(".btn-multi"),
    navi:$(".ui-navi"),
    navi_cover:$(".ui-navi-cover"),
    navi_text:$(".ui-navi-text"),
    navi_stagequestion:$(".ui-navi-stagequestion"),
		stagewrapper:$(".stage"),
    bgm:$("#bgm")
	}
  //ロード
  elm.body.addClass("onload");

  //BGM
  elm.bgm.get(0).play();

	//ソロスタート
	elm.solo.on({
		click:function(){
      effectStart();
		}
	});
  //マルチプレイスタート
  elm.multi.on({
    click:function(){
      game.is_solo = false;
      effectStart();
    }
  });

  function effectStart(){
    elm.navi_cover.animate({
      top:0,
    },300,function(){
      gameStart();
    }).animate({
      height:50
    },300,function(){
      elm.navi_text.text("ステージ1").show();
      elm.navi_stagequestion.text("タップで赤に塗りつぶせ!").show();
    });
  }

	//ゲームスタート
	function gameStart(){
		stageindex = 0;
		elm.container.attr("data-status","stage");
		if(game.is_solo){
			stageinit();
		}else{
			game.is_waitgame = true;
		}
	}

	//ステージ初期化
	function stageinit(){
    stageindex = 1;
		//データロード
		$.ajax({
			url:"assets/stage/stage"+stageindex+".html",
		}).done(function(data){
			//console.log(data);
			elm.stagewrapper.empty().append(data);
			stagelist[stageindex].init(game);
		});
	}

	//ステージ課題クリア
	function stageQuestion(i){
		stagelist[stageindex].question(i);
	}

	//ステージクリア
	function stageclear(){
		stagelist[stageindex].clear();
	}

	//


	/*----------------------------------*/

    function SigV4Utils(){}

    SigV4Utils.sign = function(key, msg) {
      var hash = CryptoJS.HmacSHA256(msg, key);
      return hash.toString(CryptoJS.enc.Hex);
    };

    SigV4Utils.sha256 = function(msg) {
      var hash = CryptoJS.SHA256(msg);
      return hash.toString(CryptoJS.enc.Hex);
    };

    SigV4Utils.getSignatureKey = function(key, dateStamp, regionName, serviceName) {
      var kDate = CryptoJS.HmacSHA256(dateStamp, 'AWS4' + key);
      var kRegion = CryptoJS.HmacSHA256(regionName, kDate);
      var kService = CryptoJS.HmacSHA256(serviceName, kRegion);
      var kSigning = CryptoJS.HmacSHA256('aws4_request', kService);
      return kSigning;
    };

    function createEndpoint(regionName, awsIotEndpoint, accessKey, secretKey) {
      var time = moment.utc();
      var dateStamp = time.format('YYYYMMDD');
      var amzdate = dateStamp + 'T' + time.format('HHmmss') + 'Z';
      var service = 'iotdevicegateway';
      var region = regionName;
      var secretKey = secretKey;
      var accessKey = accessKey;
      var algorithm = 'AWS4-HMAC-SHA256';
      var method = 'GET';
      var canonicalUri = '/mqtt';
      var host = awsIotEndpoint;

      var credentialScope = dateStamp + '/' + region + '/' + service + '/' + 'aws4_request';
      var canonicalQuerystring = 'X-Amz-Algorithm=AWS4-HMAC-SHA256';
      canonicalQuerystring += '&X-Amz-Credential=' + encodeURIComponent(accessKey + '/' + credentialScope);
      canonicalQuerystring += '&X-Amz-Date=' + amzdate;
      canonicalQuerystring += '&X-Amz-SignedHeaders=host';

      var canonicalHeaders = 'host:' + host + '\n';
      var payloadHash = SigV4Utils.sha256('');
      var canonicalRequest = method + '\n' + canonicalUri + '\n' + canonicalQuerystring + '\n' + canonicalHeaders + '\nhost\n' + payloadHash;

      var stringToSign = algorithm + '\n' +  amzdate + '\n' +  credentialScope + '\n' +  SigV4Utils.sha256(canonicalRequest);
      var signingKey = SigV4Utils.getSignatureKey(secretKey, dateStamp, region, service);
      var signature = SigV4Utils.sign(signingKey, stringToSign);

      canonicalQuerystring += '&X-Amz-Signature=' + signature;
      return 'wss://' + host + canonicalUri + '?' + canonicalQuerystring;
    }

    var endpoint = createEndpoint(
        'ap-northeast-1', // Your Region
        'a1di20ut36b9i.iot.ap-northeast-1.amazonaws.com', // Require 'lowercamelcase'!!
        'AKIAJLVIK4BDUNBGOVHQ',
        'DzPSNhboRbnluK6xnh/64yBVyiFs8GWWRrVODvyH');
    var clientId = Math.random().toString(36).substring(7);
    var client = new Paho.MQTT.Client(endpoint, clientId);
    var connectOptions = {
      useSSL: true,
      timeout: 3,
      mqttVersion: 4,
      onSuccess: subscribe
    };
    client.connect(connectOptions);
    client.onMessageArrived = onMessage;
    client.onConnectionLost = function(e) { console.log(e) };

    function subscribe() {
      client.subscribe("sharetouch/status");
      console.log("subscribed");
    }

    function send(content) {
      var message = new Paho.MQTT.Message(JSON.stringify(content));
      message.destinationName = "sharetouch/status";
      client.send(message);
      console.log("sent");
    }

    function onMessage(message) {
      data.messages.push(JSON.parse(message.payloadString));
      console.log("message received: " + message.payloadString);
      var data = JSON.parse(message.payloadString);
      stagelist[stageindex].receive(data);
      if(game.is_waitgame){
      	stageindex = data.stage;
      	stageinit();
      }
    }
});