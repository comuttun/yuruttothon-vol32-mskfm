const gulp = require('gulp'),
	sass = require("gulp-sass"),
	plumber = require("gulp-plumber"),
	rename = require('gulp-rename'),
	autoprefixer = require('gulp-autoprefixer'),
	browserSync = require("browser-sync"),
	cssmin = require("gulp-cssmin");

const reload = browserSync.reload;

const conf = {
	public: "./src/"
}

gulp.task('default',['browserSync'],function(){
	gulp.watch(conf.public+"**/scss/*.scss",["sass"]);
});

gulp.task('browserSync',function(){
	browserSync.init({
		server: {
			"baseDir":conf.public
		}
	});
});

gulp.task('sass',function() {
	gulp.src(conf.public+"**/scss/style.scss")
		.pipe(plumber({
			errorHandler: function(err) {
				console.log(err.messageFormatted);
				this.emit('end');
			}
		}))
		.pipe(sass())
		.pipe(autoprefixer({
			browsers: [ 'last 2 versions', 'ie 9' ],
            cascade: false
		}))
		.pipe(cssmin())
		.pipe(browserSync.stream())
		.pipe(rename(function (path) {
			path.dirname = path.dirname.replace('scss','css');
		}))
		.pipe(gulp.dest(conf.public));
});