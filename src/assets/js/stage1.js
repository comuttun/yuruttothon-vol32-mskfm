function stage1(){
  var game;

  function Tile(x, y) {
    return {
      x: x,
      y: y,
      clicked: false,
      click: function() {
        this.clicked = true;
      },
      isClicked: function() {
        return this.clicked;
      }
    };
  }

  this.init = function(data){
    game = data;
    game.stage1Data = {};

    function createTile(x, y) {
      return $('<span class="tile"></span>').data('x', x).data('y', y);
    }

    function createRow() {
      return $('<div class="row"></div>');
    }

    var tilesContainer = $(".stage1-container .tiles-container");
    game.stage1Data = {
      clickedCount: 0,
      tiles: [],
      rowCount: 3,
      colCount: 5,
      cleared: false,
      updateCleared: function() {
        this.cleared = this.rowCount * this.colCount == this.clickedCount;
      }
    };

    for (var i = 0; i < game.stage1Data.rowCount; i++) {
      var row = createRow();
      for (var j = 0; j < game.stage1Data.colCount; j++) {
        row.append(createTile(j, i));
        game.stage1Data.tiles[i * game.stage1Data.colCount + j] = Tile(j, i);
      }
      tilesContainer.append(row);
    }

    $(".tile").on({
      click:function(event){
        var clickedTile = $(event.target);

        console.log('clicked', clickedTile.data('x'), clickedTile.data('y'));
        var x = parseInt(clickedTile.data('x'));
        var y = parseInt(clickedTile.data('y'));
        var tile = game.stage1Data.tiles[y * game.stage1Data.colCount + x];
        if (!tile.isClicked()) {
          game.stage1Data.clickedCount += 1;
          tile.click();
          clickedTile.css('background-color', 'red');
        }

        game.stage1Data.updateCleared();

        this.submit();
      }.bind(this)
    })
  }
  this.submit = function(){
    if(game.is_solo){
      this.receive({stage:1, data: game.stage1Data})
    }else{
      game.send({stage:1, data: game.stage1Data})
    }
  }
  this.receive = function(data){
    console.log(data);
    if (data.data.cleared) {
      //alert('clear!');
      this.clear();
    }
  }

  this.clear = function(){
    game.clear();
  }

  return this;
}